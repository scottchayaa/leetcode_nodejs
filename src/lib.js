const ListNode = require('./ListNode');


// 方案一
// function lib() {}

// lib.prototype = {
//     convertAryToListNodes: function (ary) {
//         let now_head = new ListNode();
//         let head = now_head;
//         ary.forEach(val => {
//             now_head.next = new ListNode(val);
//             now_head = now_head.next;
//         });
//         return head.next;
//     },
//     convertListNodesToAry: function (ListNode) {
//         let ary = [];
//         let now_head = ListNode;
//         while (now_head) {
//             ary.push(now_head.val);
//             now_head = now_head.next;
//         }
//         return ary;
//     }
// };

// module.exports = lib;


// 方案二
// module.exports = {
//     convertAryToListNodes(ary) {
//         let now_head = new ListNode();
//         let head = now_head;
//         ary.forEach(val => {
//             now_head.next = new ListNode(val);
//             now_head = now_head.next;
//         });
//         return head.next;
//     },

//     convertListNodesToAry(ListNode) {
//         let ary = [];
//         let now_head = ListNode;
//         while (now_head) {
//             ary.push(now_head.val);
//             now_head = now_head.next;
//         }
//         return ary;
//     }
// }


// 方案三
exports.convertAryToListNodes = (ary) => {
    let now_head = new ListNode();
    let head = now_head;
    ary.forEach(val => {
        now_head.next = new ListNode(val);
        now_head = now_head.next;
    });
    return head.next;
}

exports.convertListNodesToAry = (ListNode) => {
    let ary = [];
    let now_head = ListNode;
    while (now_head) {
        ary.push(now_head.val);
        now_head = now_head.next;
    }
    return ary;
}