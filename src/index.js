const lib = require('./lib');

// let lib = new _lib;
// console.log(lib1.aaa());

/**
 * How to use ListNode.
 */
let _ary1 = [2,5,6];
let _ary2 = [3,5,1,4,4];
let nodeList1 = lib.convertAryToListNodes(_ary1);
let nodeList2 = lib.convertAryToListNodes(_ary2);
let ary1 = lib.convertListNodesToAry(nodeList1);
let ary2 = lib.convertListNodesToAry(nodeList2);

console.log('===');
console.log(nodeList1);
console.log(nodeList2);
console.log('===');
console.log(ary1);
console.log(ary2);

