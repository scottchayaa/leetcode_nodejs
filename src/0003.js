let s = "dvdf";

let s_ary = [...s];

let max_pool = [];

while (s_ary.length) {
    let pool = [];

    for (let i = 0; i < s_ary.length; i++) {
        let char = s_ary[i];
        if (pool.includes(char)) {
            // console.log(pool.join(''), char);
            break;
        } else {
            pool.push(char);
        }
    }
    
    if (pool.length > max_pool.length) {
        max_pool = pool;
    }

    s_ary.shift();
}


console.log("=== result ===");
console.log(max_pool.join(''));
console.log(max_pool.length);
